package zad5;

import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

import service.*;
import domain.*;

public class UserServiceTest extends TestCase {
	
	@Test(expected=NullPointerException.class)
	public void col_services_should_throw_exception_if_null_is_entered_as_a_parameter(){
		UserService.findUsersWhoHaveMoreThanOneAddress(null);
		UserService.findOldestPerson(null);
		UserService.findUserWithLongestUsername(null);
		UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(null);
		UserService.getSortedPermissionsOfUsersWithNameStartingWithA(null);
		UserService.groupUsersByRole(null);
		UserService.partitionUserByUnderAndOver18(null);
	}

	
}