package service;

import java.util.ArrayList;
import java.util.List;

import domain.*;

public class Main {

	public static void main(String[] args) {

		
		List <User> users = new ArrayList<User>();
		
		Person person1 = new Person();
		Person person2 = new Person();
		Person person3 = new Person();
		
		person1.setName("Olek");
		person2.setName("Jarek");
		person3.setName("Leszek");
		
		person1.setSurname("Flaszka");
		person2.setSurname("Ptasiński");
		person3.setSurname("Solidarski");
		
		person1.setAge(18);
		person2.setAge(20);
		person3.setAge(15);
		
		Role role1 = new Role();
		Role role2 = new Role();
		Role role3 = new Role();
		
		Permission perm1 = new Permission();
		Permission perm2 = new Permission();
		Permission perm3 = new Permission();
		Permission perm4 = new Permission();
		Permission perm5 = new Permission();
		Permission perm6 = new Permission();
		
		perm1.setName("aaa1");
		perm2.setName("bbb1");
		perm3.setName("ccc1");
		perm4.setName("aaa2");
		perm5.setName("bbb2");
		perm6.setName("aaa3");
		
		role1.getPermissions().add(perm1);
		role1.getPermissions().add(perm2);
		role1.getPermissions().add(perm3);
		role2.getPermissions().add(perm4);
		role2.getPermissions().add(perm5);
		role3.getPermissions().add(perm6);
		
		person1.setRole(role1);
		person2.setRole(role2);
		person3.setRole(role3);
		
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		
		user1.setName("a");
		user2.setName("ab");
		user3.setName("abc");
		
		Address address1 = new Address();
		Address address2 = new Address();
		Address address3 = new Address();
		
		user1.setPersonDetails(person1);
		user2.setPersonDetails(person2);
		user3.setPersonDetails(person3);
		
		user1.getPersonDetails().getAddresses().add(address1);
		user1.getPersonDetails().getAddresses().add(address2);
		user2.getPersonDetails().getAddresses().add(address3);

		users.add(user1);
		users.add(user2);
		users.add(user3);
		
		System.out.println(UserService.findUsersWhoHaveMoreThanOneAddress(users));
		
		System.out.println(UserService.findOldestPerson(users).getAge());
		
		System.out.println(UserService.findUserWithLongestUsername(users).getName());
		
		System.out.println(UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users));
		
		System.out.println(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		System.out.println(UserService.groupUsersByRole(users));
		
		System.out.println(UserService.partitionUserByUnderAndOver18(users));
		
		
	}

}