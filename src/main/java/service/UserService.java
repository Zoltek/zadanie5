package service;

import domain.*;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UserService {


    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {

    	if(users==null)
			throw new NullPointerException();
    	
    	List<User> moreThanOneAddress = users.stream()
        	.filter(user -> user.getPersonDetails().getAddresses().size() > 1)
        	.collect(Collectors.toList());
        
    	return moreThanOneAddress;

    }

    public static Person findOldestPerson(List<User> users) {
    	
    		User oldestPerson = users.stream()
    		.max(Comparator.comparing(user -> user.getPersonDetails().getAge())).get();
    		
    		return oldestPerson.getPersonDetails();
    	        
    }

    public static User findUserWithLongestUsername(List<User> users) {
    	
    	User longestUsername = users.stream()
    		.max(Comparator.comparing(User::getName))
    		.get();
    	
    	return longestUsername;
    	    
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

    	
    	return users.stream()
    		.filter(user -> user.getPersonDetails().getAge() >= 18)
    		.map(user -> user.getPersonDetails()
    		.getName() + " " + user.getPersonDetails().getSurname())
            .collect(Collectors.joining(", "));
    	
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        
    	List<List<Permission>> listOfListsPermissions = users.stream()
                .filter(user -> user.getPersonDetails().getName().startsWith("A"))
                .map(user -> user.getPersonDetails().getRole().getPermissions())
                .collect(Collectors.toList());
    			
    	List<Permission> listOfPermissions = 
    		    listOfListsPermissions.stream()
    		    .flatMap(List::stream)
    		    .collect(Collectors.toList());
    	   
        return listOfPermissions.stream()
        		.map(Permission::getName)
                .distinct()
                .sorted()
                .collect(Collectors.toList()); 
    	
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
    	
    	List<List<Permission>> listOfListsPermissions = users.stream()
                .filter(user -> user.getPersonDetails().getSurname().startsWith("S"))
                .map(user -> user.getPersonDetails().getRole().getPermissions())
                .collect(Collectors.toList());

    		    listOfListsPermissions.stream()
    		        .flatMap(List::stream)
    		        .forEach(Permission -> System.out.println(Permission.getName()));
    		    
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        
    	Map<Role, List<User>> usersByRole = users.stream()
    			.collect(Collectors.groupingBy(	user -> user.getPersonDetails().getRole()));
    	
    		return usersByRole;
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
    	
    	Map<Boolean, List<User>> underAndOver18 = users.stream()
    			.filter(user -> user.getPersonDetails().getAge() > 0)
    			.collect(Collectors.groupingBy(user -> user.getPersonDetails().getAge() >= 18));
    	
    		return underAndOver18;
    }
}