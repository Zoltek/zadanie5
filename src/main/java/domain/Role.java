package domain;

import java.util.List;
import java.util.ArrayList;;

public class Role {

    private String name;
    List<Permission> permissions;
    
    public Role() {
		permissions = new ArrayList<Permission>();
	}

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public Role setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
        return this;
    }
}